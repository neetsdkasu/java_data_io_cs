﻿using System;

namespace JavaDataIO
{
    public sealed class UTFDataFormatException : Exception
    {
        public UTFDataFormatException()
        {
        }

        public UTFDataFormatException(string message)
            : base(message)
        {
        }

        public UTFDataFormatException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}