﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace JavaDataIO
{
    public sealed class JavaDataInput : IDisposable
    {
        private readonly Stream _stream;
        private bool _hasUnread = false;
        private byte _unreadByte = 0;
        private byte[] _buf = new byte[8];

        public JavaDataInput(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }
            if (!stream.CanRead)
            {
                throw new ArgumentException("stream must be readable", "stream");
            }
            _stream = stream;
        }

        public void Dispose()
        {
            _stream.Dispose();
        }

        private void Read(byte[] buf, int offset, int length)
        {
            if (_hasUnread)
            {
                buf[offset] = _unreadByte;
                _hasUnread = false;
                offset++;
                length--;
            }
            while (length > 0)
            {
                int n = _stream.Read(buf, offset, length);
                if (n == 0)
                {
                    throw new EndOfStreamException();
                }
                offset += n;
                length -= n;
            }
        }

        public void ReadFully(byte[] buf)
        {
            ReadFully(buf, 0, buf?.Length ?? 0);
        }

        public void ReadFully(byte[] buf, int offset, int length)
        {
            if (buf == null)
            {
                throw new ArgumentNullException("buf");
            }
            if (offset < 0)
            {
                throw new ArgumentOutOfRangeException("offset");
            }
            if (length < 0)
            {
                throw new ArgumentOutOfRangeException("length");
            }
            if (offset + length > buf.Length)
            {
                throw new ArgumentException("must be offset + length <= buf.Length");
            }
            Read(buf, offset, length);
        }

        public double ReadDouble()
        {
            long v = ReadLong();
            return BitConverter.Int64BitsToDouble(v);
        }

        public float ReadFloat()
        {
            int v = ReadInt();
#if (NET5_0 || NETSTANDARD2_1)
            return BitConverter.Int32BitsToSingle(v);
#else
            return BitConverter.ToSingle(BitConverter.GetBytes(v), 0);
#endif
        }

        public long ReadLong()
        {
            Read(_buf, 0, 8);
            long v = BitConverter.ToInt64(_buf, 0);
            return IPAddress.HostToNetworkOrder(v);
        }

        public int ReadInt()
        {
            Read(_buf, 0, 4);
            int v = BitConverter.ToInt32(_buf, 0);
            return IPAddress.HostToNetworkOrder(v);
        }

        public short ReadShort()
        {
            Read(_buf, 0, 2);
            short v = BitConverter.ToInt16(_buf, 0);
            return IPAddress.HostToNetworkOrder(v);
        }

        public ushort ReadUnsignedShort()
        {
            return unchecked((ushort)ReadShort());
        }

        public sbyte ReadByte()
        {
            Read(_buf, 0, 1);
            return unchecked((sbyte)_buf[0]);
        }

        public byte ReadUnsignedByte()
        {
            Read(_buf, 0, 1);
            return _buf[0];
        }

        public bool ReadBoolean()
        {
            Read(_buf, 0, 1);
            return BitConverter.ToBoolean(_buf, 0);
        }

        public char ReadChar()
        {
            return (char)ReadUnsignedShort();
        }

        public string ReadUTF()
        {
            var count = (int)ReadUnsignedShort();
            var sb = new StringBuilder();
            while (count > 0)
            {
                Read(_buf, 0, 1);
                if ((_buf[0] & 0xF0) == 0xF0)
                {
                    throw new UTFDataFormatException();
                }
                if ((_buf[0] & 0xC0) == 0x80)
                {
                    throw new UTFDataFormatException();
                }
                if (_buf[0] < 0x80)
                {
                    sb.Append((char)_buf[0]);
                    count--;
                }
                else if (_buf[0] < (0x80 | 0x40 | 0x20) && count >= 2)
                {
                    Read(_buf, 1, 1);
                    if ((_buf[1] & 0xC0) == 0x80)
                    {
                        int a = (int)(_buf[0] & 0x1F) << 6;
                        int b = (int)(_buf[1] & 0x3F);
                        sb.Append((char)(a | b));
                        count -= 2;
                    }
                    else
                    {
                        throw new UTFDataFormatException();
                    }
                }
                else if (count >= 3)
                {
                    Read(_buf, 1, 2);
                    if ((_buf[1] & 0xC0) == 0x80 && (_buf[2] & 0xC0) == 0x80)
                    {
                        int a = (int)(_buf[0] & 0x0F) << 12;
                        int b = (int)(_buf[1] & 0x3F) << 6;
                        int c = (int)(_buf[2] & 0x3F);
                        sb.Append((char)(a | b | c));
                        count -= 3;
                    }
                    else
                    {
                        throw new UTFDataFormatException();
                    }
                }
                else
                {
                    throw new UTFDataFormatException();
                }
            }
            return sb.ToString();
        }

        public string ReadLine()
        {
            var sb = new StringBuilder();
            if (_hasUnread)
            {
                _hasUnread = false;
                if (_unreadByte == '\n')
                {
                    return "";
                }
                if (_unreadByte == '\r')
                {
                    int n = _stream.Read(_buf, 0, 1);
                    if (n != 0 && _buf[0] != '\n')
                    {
                        _unreadByte = _buf[0];
                        _hasUnread = true;
                    }
                    return "";
                }
                sb.Append((char)_unreadByte);
            }
            for (; ; )
            {
                int n = _stream.Read(_buf, 0, 1);
                if (n == 0)
                {
                    break;
                }
                byte b = _buf[0];
                if (b == '\n')
                {
                    break;
                }
                if (b == '\r')
                {
                    n = _stream.Read(_buf, 0, 1);
                    if (n != 0 && _buf[0] != '\n')
                    {
                        _hasUnread = true;
                        _unreadByte = _buf[0];
                    }
                    break;
                }
                sb.Append((char)b);
            }
            return sb.ToString();
        }

        public int SkipBytes(int n)
        {
            var buf = new byte[Math.Min(4096, n)];
            var skipped = 0;
            while (skipped < n)
            {
                int x = _stream.Read(buf, 0, Math.Min(buf.Length, n - skipped));
                if (x == 0)
                {
                    return skipped;
                }
                skipped += x;
            }
            return skipped;
        }
    }
}