﻿using System;
using System.IO;
using System.Net;

namespace JavaDataIO
{
    public class JavaDataOutput : IDisposable
    {
        private readonly Stream _stream;

        public JavaDataOutput(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }
            if (!stream.CanWrite)
            {
                throw new ArgumentException("stream must be writable", "stream");
            }
            _stream = stream;
        }

        public void Dispose()
        {
            _stream.Dispose();
        }

        public void Write(byte[] buf)
        {
            Write(buf, 0, buf?.Length ?? 0);
        }

        public void Write(byte[] buf, int offset, int length)
        {
            if (buf == null)
            {
                throw new ArgumentNullException("buf");
            }
            if (offset < 0)
            {
                throw new ArgumentOutOfRangeException("offset");
            }
            if (length < 0)
            {
                throw new ArgumentOutOfRangeException("length");
            }
            if (offset + length > buf.Length)
            {
                throw new ArgumentException("must be offset + length <= buf.Length");
            }
            _stream.Write(buf, offset, length);
        }

        public void Write(int b)
        {
            _stream.WriteByte(unchecked((byte)b));
        }

        public void WriteBoolean(bool v)
        {
            _stream.WriteByte(v ? (byte)1 : (byte)0);
        }

        public void WriteByte(int v)
        {
            _stream.WriteByte(unchecked((byte)v));
        }

        public void WriteBytes(string s)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }
            foreach (char ch in s)
            {
                WriteByte((int)ch);
            }
        }

        public void WriteChar(int v)
        {
            WriteByte(v >> 8);
            WriteByte(v);
        }

        public void WriteChars(string s)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }
            foreach (char ch in s)
            {
                WriteChar((int)ch);
            }
        }

        public void WriteDouble(double v)
        {
            WriteLong(BitConverter.DoubleToInt64Bits(v));
        }

        public void WriteFloat(float v)
        {
#if (NET5_0 || NETSTANDARD2_1)
            WriteInt(BitConverter.SingleToInt32Bits(v));
#else
            byte[] buf = BitConverter.GetBytes(v);
            WriteInt(BitConverter.ToInt32(buf, 0));
#endif
        }

        public void WriteInt(int v)
        {
            WriteByte(v >> 24);
            WriteByte(v >> 16);
            WriteByte(v >> 8);
            WriteByte(v);
        }

        public void WriteLong(long v)
        {
            _stream.WriteByte(unchecked((byte)(v >> 56)));
            _stream.WriteByte(unchecked((byte)(v >> 48)));
            _stream.WriteByte(unchecked((byte)(v >> 40)));
            _stream.WriteByte(unchecked((byte)(v >> 32)));
            _stream.WriteByte(unchecked((byte)(v >> 24)));
            _stream.WriteByte(unchecked((byte)(v >> 16)));
            _stream.WriteByte(unchecked((byte)(v >> 8)));
            _stream.WriteByte(unchecked((byte)v));
        }

        public void WriteShort(int v)
        {
            WriteByte(v >> 8);
            WriteByte(v);
        }

        public void WriteUTF(string s)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }
            var count = 0;
            foreach (char ch in s)
            {
                int c = (int)ch;
                if (0 < c && c < 0x0080)
                {
                    count++;
                }
                else if (c < 0x0800)
                {
                    count += 2;
                }
                else
                {
                    count += 3;
                }
            }
            if (count > 0xFFFF)
            {
                throw new UTFDataFormatException();
            }
            WriteShort(count);
            foreach (char ch in s)
            {
                int c = (int)ch;
                if (0 < c && c < 0x0080)
                {
                    WriteByte(c);
                }
                else if (c < 0x0800)
                {
                    WriteByte(0xC0 | (0x1F & (c >> 6)));
                    WriteByte(0x80 | (0x3F & c));
                }
                else
                {
                    WriteByte(0xE0 | (0x0F & (c >> 12)));
                    WriteByte(0x80 | (0x3F & (c >> 6)));
                    WriteByte(0x80 | (0x3F & c));
                }
            }
        }
    }
}