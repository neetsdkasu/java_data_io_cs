using JavaDataIO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;

namespace JavaDataIOTest
{
    [TestClass]
    public class UnitTest1
    {
        private byte[] ToBytes(sbyte[] data)
        {
            return Array.ConvertAll(data, (v) => unchecked((byte)v));
        }

        [TestMethod]
        public void TestReadFully()
        {
            var buf = new byte[] { 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0 };
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            var dst = new byte[8];
            dis.ReadFully(dst);
            CollectionAssert.AreEqual(buf, dst);
        }

        [TestMethod]
        [ExpectedException(typeof(EndOfStreamException))]
        public void TestReadFullyFailed1()
        {
            var buf = new byte[] { 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0 };
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            var dst = new byte[100];
            dis.ReadFully(dst);
            CollectionAssert.AreEqual(buf, dst);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestReadFullyFailed2()
        {
            var buf = new byte[] { 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0 };
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            dis.ReadFully(null);
        }

        [TestMethod]
        public void TestReadLong()
        {
            var buf = new byte[] { 0xF2, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0 };
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            long v = dis.ReadLong();
            Assert.AreEqual(unchecked((long)0xF234_5678_9ABC_DEF0UL), v);
        }

        [TestMethod]
        [ExpectedException(typeof(EndOfStreamException))]
        public void TestReadLongFailed()
        {
            var buf = new byte[] { 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE };
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            dis.ReadLong();
        }

        [TestMethod]
        public void TestReadDouble()
        {
            byte[] buf = ToBytes(new sbyte[] { -56, 66, 35, -19, -94, 68, -7, -66 });
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            double v = dis.ReadDouble();
            Assert.AreEqual(-1.23456789e40, v);
        }

        [TestMethod]
        public void TestReadFloat()
        {
            byte[] buf = ToBytes(new sbyte[] { -80, -121, -67, -1 });
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            float v = dis.ReadFloat();
            Assert.AreEqual(-9.87654321e-10F, v);
        }

        [TestMethod]
        public void TestReadInt()
        {
            var buf = new byte[] { 0xF2, 0x34, 0x56, 0x78 };
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            int v = dis.ReadInt();
            Assert.AreEqual(unchecked((int)0xF234_5678U), v);
        }

        [TestMethod]
        public void TestReadShort()
        {
            var buf = new byte[] { 0xF1, 0x34 };
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            short v = dis.ReadShort();
            Assert.AreEqual(unchecked((short)0xF134), v);
        }

        [TestMethod]
        public void TestReadUnsignedShort()
        {
            var buf = new byte[] { 0xF1, 0x34 };
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            ushort v = dis.ReadUnsignedShort();
            Assert.AreEqual((ushort)0xF134, v);
        }

        [TestMethod]
        public void TestReadByte()
        {
            var buf = new byte[] { 0xF1 };
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            sbyte v = dis.ReadByte();
            Assert.AreEqual(unchecked((sbyte)0xF1), v);
        }

        [TestMethod]
        public void TestReadUnsignedByte()
        {
            var buf = new byte[] { 0xF1 };
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            byte v = dis.ReadUnsignedByte();
            Assert.AreEqual((byte)0xF1, v);
        }

        [TestMethod]
        [DataRow(0, false)]
        [DataRow(1, true)]
        [DataRow(2, true)]
        [DataRow(255, true)]
        public void TestReadBoolean(int b, bool r)
        {
            var buf = new byte[] { (byte)b };
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            bool v = dis.ReadBoolean();
            Assert.AreEqual(r, v);
        }

        [TestMethod]
        public void TestReadChar()
        {
            var buf = new byte[] { 0x00, 0x6A };
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            char v = dis.ReadChar();
            Assert.AreEqual('\u006A', v);
        }

        [TestMethod]
        public void TestReadUTF()
        {
            byte[] buf = { 0, 38, 227, 129, 130, 227, 129, 132, 227, 129, 134, 68, 120, 232, 171, 184, 232, 161, 140, 231, 132, 161, 229, 184, 184, 239, 188, 160, 227, 130, 184, 227, 131, 163, 227, 131, 180, 227, 130, 161 };
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            string s = dis.ReadUTF();
            Assert.AreEqual("あいうDx諸行無常＠ジャヴァ", s);
        }

        [TestMethod]
        public void TestReadLine()
        {
            var buf = new byte[] {
                (byte)'f', (byte)'o', (byte)'o', (byte)'\n', (byte)'\n',
                (byte)'b', (byte)'a', (byte)'r', (byte)'\r',
                (byte)'b', (byte)'a', (byte)'z', (byte)'\r', (byte)'\n',
                (byte)'j', (byte)'a', (byte)'v', (byte)'a'
            };
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            string s = dis.ReadLine();
            Assert.AreEqual("foo", s);
            s = dis.ReadLine();
            Assert.AreEqual("", s);
            s = dis.ReadLine();
            Assert.AreEqual("bar", s);
            s = dis.ReadLine();
            Assert.AreEqual("baz", s);
            s = dis.ReadLine();
            Assert.AreEqual("java", s);
            s = dis.ReadLine();
            Assert.AreEqual("", s);
        }

        [TestMethod]
        public void TestSkipBytes()
        {
            var buf = new byte[100];
            var mem = new MemoryStream(buf);
            var dis = new JavaDataInput(mem);
            var n = dis.SkipBytes(80);
            Assert.AreEqual(n, 80);
            n = dis.SkipBytes(80);
            Assert.AreEqual(20, n);
        }

        [TestMethod]
        public void TestWrite()
        {
            var src = new byte[] { 1, 2, 3, 4, 5 };
            var mem = new MemoryStream();
            var dos = new JavaDataOutput(mem);
            dos.Write(1);
            dos.Write(src, 1, 3);
            dos.Write(5);
            CollectionAssert.AreEqual(src, mem.ToArray());
        }

        [TestMethod]
        public void TestWriteBoolean()
        {
            var src = new byte[] { 1, 0 };
            var mem = new MemoryStream();
            var dos = new JavaDataOutput(mem);
            dos.WriteBoolean(true);
            dos.WriteBoolean(false);
            CollectionAssert.AreEqual(src, mem.ToArray());
        }

        [TestMethod]
        public void TestWriteByte()
        {
            var src = new byte[] { 1, 2, 3, 4, 5 };
            var mem = new MemoryStream();
            var dos = new JavaDataOutput(mem);
            for (var i = 1; i <= 5; i++)
            {
                dos.WriteByte(i);
            }
            CollectionAssert.AreEqual(src, mem.ToArray());
        }

        [TestMethod]
        public void TestWriteBytes()
        {
            var src = new byte[] { (byte)'A', (byte)'B', (byte)'C' };
            var mem = new MemoryStream();
            var dos = new JavaDataOutput(mem);
            dos.WriteBytes("ABC");
            CollectionAssert.AreEqual(src, mem.ToArray());
        }

        [TestMethod]
        public void TestWriteChar()
        {
            var src = new byte[] { 0, (byte)'A', 0, (byte)'B', 0, (byte)'C' };
            var mem = new MemoryStream();
            var dos = new JavaDataOutput(mem);
            foreach (char ch in "ABC")
            {
                dos.WriteChar(ch);
            }
            CollectionAssert.AreEqual(src, mem.ToArray());
        }

        [TestMethod]
        public void TestWriteChars()
        {
            var src = new byte[] { 0, (byte)'A', 0, (byte)'B', 0, (byte)'C' };
            var mem = new MemoryStream();
            var dos = new JavaDataOutput(mem);
            dos.WriteChars("ABC");
            CollectionAssert.AreEqual(src, mem.ToArray());
        }

        [TestMethod]
        public void TestWriteDouble()
        {
            byte[] src = ToBytes(new sbyte[] { -56, 66, 35, -19, -94, 68, -7, -66 });
            var mem = new MemoryStream();
            var dos = new JavaDataOutput(mem);
            dos.WriteDouble(-1.23456789e40);
            CollectionAssert.AreEqual(src, mem.ToArray());
        }

        [TestMethod]
        public void TestWriteFloat()
        {
            byte[] src = ToBytes(new sbyte[] { -80, -121, -67, -1 });
            var mem = new MemoryStream();
            var dos = new JavaDataOutput(mem);
            dos.WriteFloat(-9.87654321e-10F);
            CollectionAssert.AreEqual(src, mem.ToArray());
        }

        [TestMethod]
        public void TestWriteInt()
        {
            var src = new byte[] { 0xF1, 0x34, 0x56, 0x78 };
            var mem = new MemoryStream();
            var dos = new JavaDataOutput(mem);
            dos.WriteInt(unchecked((int)0xF134_5678U));
            CollectionAssert.AreEqual(src, mem.ToArray());
        }

        [TestMethod]
        public void TestWriteLong()
        {
            var src = new byte[] { 0xF1, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF };
            var mem = new MemoryStream();
            var dos = new JavaDataOutput(mem);
            dos.WriteLong(unchecked((long)0xF123_4567_89AB_CDEFUL));
            CollectionAssert.AreEqual(src, mem.ToArray());
        }

        [TestMethod]
        public void TestWriteShort()
        {
            var src = new byte[] { 0xF1, 0x23 };
            var mem = new MemoryStream();
            var dos = new JavaDataOutput(mem);
            dos.WriteShort(unchecked((short)0xF123));
            CollectionAssert.AreEqual(src, mem.ToArray());
        }

        [TestMethod]
        public void TestWriteUTF()
        {
            byte[] src = { 0, 38, 227, 129, 130, 227, 129, 132, 227, 129, 134, 68, 120, 232, 171, 184, 232, 161, 140, 231, 132, 161, 229, 184, 184, 239, 188, 160, 227, 130, 184, 227, 131, 163, 227, 131, 180, 227, 130, 161 };
            var mem = new MemoryStream();
            var dos = new JavaDataOutput(mem);
            dos.WriteUTF("あいうDx諸行無常＠ジャヴァ");
            CollectionAssert.AreEqual(src, mem.ToArray());
        }
    }
}